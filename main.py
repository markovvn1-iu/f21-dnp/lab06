"""Simple implementation of the Chord protocol (D&NP lab06)

Usage:
    main.py [-h] [-registry_port PORT] [-m BITS] FIRST_PORT LAST_PORT

    positional arguments:
        FIRST_PORT           port range for nodes
        LAST_PORT            port range for nodes

    optional arguments:
        -h, --help           show help message and exit
        -registry_port PORT  port for registry
        -m BITS              length of identifiers in chord (in bits)


Examples:
    Use default value for m (5) and registry port (25565)
    $ python main.py 5000 5004

    Specify value of m
    $ python main.py -m 6 5000 5004

    Specify value of m and registry port
    $ python main.py -m 6 -registry_port 25566 5000 5004
"""


import argparse
import random
import time

from node import Node
from registry import Register
from proxy import NodeProxy, RegisterProxy


def parse_args(args=None):
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description='Simple implementation of the Chord protocol (D&NP lab06)',
        add_help=True,
    )

    parser.add_argument('first_port', type=int, metavar='FIRST_PORT', help='port range for nodes')
    parser.add_argument('last_port', type=int, metavar='LAST_PORT', help='port range for nodes')
    parser.add_argument('-registry_port', type=int, metavar='PORT', default=25565, help='port for registry')
    parser.add_argument('-m', type=int, metavar='BITS', default=5, help='length of identifiers in chord (in bits)')
    return parser.parse_args(args)


def main():
    args = parse_args()
    random.seed(0)

    register = Register(('localhost', args.registry_port), args.m)
    register.start()

    nodes = [Node(('localhost', p), f'http://localhost:{args.registry_port}') for p in range(args.first_port, args.last_port + 1)]
    for n in nodes:
        n.start()

    registerProxy = RegisterProxy(f'http://localhost:{register.port}')
    nodesProxy = {n.port: NodeProxy(f'http://localhost:{n.port}') for n in nodes}

    print('Registry and 5 nodes are created.')

    def parse_input(cmdline: str):
        cmd = cmdline.split()
        if cmd[0] == 'get_chord_info':
            print(registerProxy.get_chord_info())
        elif cmd[0] == 'get_finger_table' and len(cmd) == 2:
            try:
                port = int(cmd[1])
            except ValueError as e:
                raise ValueError('Incorrect port number') from e
            if port not in nodesProxy:
                raise ValueError(f'Node with port {port} does not exists')
            print(nodesProxy[port].get_finger_table())
        elif cmd[0] == 'quit' and len(cmd) == 2:
            try:
                port = int(cmd[1])
            except ValueError as e:
                raise ValueError('Incorrect port number') from e
            if port not in nodesProxy:
                raise ValueError(f'Node with port {port} does not exists')
            print(nodesProxy[port].quit())
            del nodesProxy[port]
        else:
            raise ValueError(f'Unknown command. Try again')

    try:
        while True:
            try:
                parse_input(input('> '))
            except EOFError:
                break
            except Exception as e:
                print(repr(e))
    except KeyboardInterrupt:
        print('KeyboardInterrupt')
    finally:
        for np in nodesProxy.values():
            np.quit()

        register.stop()
        register.join()

        for n in nodes:
            n.join()


if __name__ == '__main__':
    main()
