from threading import Thread
import time
from typing import Any, Dict, Tuple
from xmlrpc.server import SimpleXMLRPCServer

from proxy import RegisterProxy



class Node(Thread):

    _working: bool = False

    _node_id: int = -1
    _server_addr: Tuple[str, int]
    _server: SimpleXMLRPCServer
    _register: RegisterProxy
    _finger_table: Dict[int, int] = {}

    def __init__(self, server_addr: Tuple[str, int], register_uri: str):
        super().__init__()
        self._server_addr = server_addr
        self._register = RegisterProxy(register_uri)

        self._server = SimpleXMLRPCServer(server_addr, logRequests=False)
        self._server.register_introspection_functions()
        self._server.register_function(self.get_finger_table)
        self._server.register_function(self.quit)

    @property
    def port(self) -> int:
        return self._server_addr[1]

    def get_finger_table(self) -> Dict[str, int]:
        return {str(k): v for k, v in self._finger_table.items()}

    def quit(self) -> Tuple[bool, str]:
        status, _ = self._register.deregister(self._node_id)
        if not status:
            return False, 'no such id exists in dict of registered nodes'
        self._working = False
        return True, 'node disconected from the network'

    def run(self):
        self._working = True
        self._node_id, _ = self._register.register(self._server_addr[1])
        time.sleep(1)
        self._finger_table = self._register.populate_finger_table(self._node_id)

        while self._working:
            self._server.handle_request()
