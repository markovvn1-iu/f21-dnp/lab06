from xmlrpc.client import ServerProxy
from typing import Any, Dict, Tuple


class NodeProxy:

    _proxy: ServerProxy

    def __init__(self, *args: Any, **kwargs: Any):
        self._proxy = ServerProxy(*args, **kwargs)

    def get_finger_table(self) -> Dict[int, int]:
        return {int(k): v for k, v in self._proxy.get_finger_table().items()}

    def quit(self) -> Tuple[bool, str]:
        return self._proxy.quit()


class RegisterProxy:

    _proxy: ServerProxy

    def __init__(self, *args: Any, **kwargs: Any):
        self._proxy = ServerProxy(*args, **kwargs)

    def register(self, port) -> Tuple[int, str]:
        return self._proxy.register(port)

    def deregister(self, node_id) -> Tuple[bool, str]:
        return self._proxy.deregister(node_id)

    def get_chord_info(self) -> Dict[int, int]:
        return {int(k): v for k, v in self._proxy.get_chord_info().items()}

    def populate_finger_table(self, node_id: int) -> Dict[int, int]:
        return {int(k): v for k, v in self._proxy.populate_finger_table(node_id).items()}
