import random
from bisect import bisect_left
from threading import Thread
from typing import Dict, Tuple
from xmlrpc.server import SimpleXMLRPCServer


class Register(Thread):

    _working: bool = False

    _m: int  # key length (in bits)
    _max_nodes: int
    _nodes_info: Dict[int, int] = {}  # {node_id: port}

    _server_addr: Tuple[str, int]
    _server: SimpleXMLRPCServer

    def __init__(self, server_addr: Tuple[str, int], m: int):
        super().__init__()
        self._m = m
        self._max_nodes = 2 ** m

        self._server_addr = server_addr
        self._server = SimpleXMLRPCServer(server_addr, logRequests=False)
        self._server.timeout = 0.1
        self._server.register_introspection_functions()
        self._server.register_function(self.register)
        self._server.register_function(self.deregister)
        self._server.register_function(self.get_chord_info)
        self._server.register_function(self.populate_finger_table)

    @property
    def port(self) -> int:
        return self._server_addr[1]

    def register(self, port) -> Tuple[int, str]:
        if len(self._nodes_info) >= self._max_nodes:
            return -1, 'network is full'

        while True:
            node_id = random.randint(0, self._max_nodes - 1)
            if node_id not in self._nodes_info:
                break

        self._nodes_info[node_id] = port
        return node_id, 'network is not full'

    def deregister(self, node_id) -> Tuple[bool, str]:
        if node_id not in self._nodes_info:
            return False, f'Node with id {node_id} is not registered'
        del self._nodes_info[node_id]
        return True, f'Node with id {node_id} is successfully deregistered'

    def get_chord_info(self) -> Dict[str, int]:
        return {str(k): v for k, v in self._nodes_info.items()}

    def populate_finger_table(self, node_id: int) -> Dict[str, int]:
        all_nodes = sorted(list(self._nodes_info.keys()))
        succ = lambda x: all_nodes[bisect_left(all_nodes, x) % len(all_nodes)]  # get successor for node x

        res_ids = [succ((node_id + 2 ** i) % self._max_nodes) for i in range(self._m)]
        return {str(i): self._nodes_info[i] for i in res_ids}

    def run(self):
        self._working = True
        while self._working:
            self._server.handle_request()

    def stop(self):
        self._working = False
